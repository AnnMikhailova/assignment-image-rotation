#include "../include/img_transformations/transf_handler.h"
#include "../include/img_transformations/rotate_left_90_trasformation.h"

typedef struct transformation_interface transf_i_type();

transf_i_type* ti_arr[] = {
    [rotate_left_90] = get_transformation_interface_rotation_90_left
};


struct image apply_transf( struct image source , enum transf_type type){
    return ti_arr[type]().transform(source);
}
