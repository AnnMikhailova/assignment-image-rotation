#include "../include/img_transformations/rotate_left_90_trasformation.h"
#include <stdio.h>


struct image rotate( struct image source ){

    uint64_t h = source.height;
    uint64_t w = source.width;

    struct image new = image_create(w, h);

    for (uint64_t i = 0; i < h; i++ ) {
        for (uint64_t j = 0; j < w; j++) {
            image_set_pixel(&new, j, h - i - 1, image_get_pixel(source, i, j));
        }
    }
    
    return new;
}

struct transformation_interface get_transformation_interface_rotation_90_left(){
    return (struct transformation_interface) {rotate};
}
