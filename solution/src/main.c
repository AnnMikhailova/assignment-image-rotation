#include "../include/data/image.h"
#include "../include/formats_handlers/bmp_format_handler.h"
#include "../include/img_transformations/transf_handler.h"
#include "../include/io/file_io_util.h"
#include <stdio.h>

enum error {
    EX_DESCRIPTION_CANNOT_OPEN_FILE_READ,
    EX_DESCRIPTION_CANNOT_OPEN_FILE_WRITE,
    EX_READ_IMG_NULL,
    EX_READ_IMG_INVALID_HEADER,
    EX_READ_IMG_INVALID_BITS,
    EX_INVALID_POSITION_SET,
    EX_WRITE_IMG_NULL,
    EX_WRITE_IMG_ERROR,
    EX_WRONG_ARGS, 
    EX_WRITE_HEADER_ERROR
};

static const char* errors_desc[] = {
        [EX_DESCRIPTION_CANNOT_OPEN_FILE_READ] = "Can't open file for read",
        [EX_DESCRIPTION_CANNOT_OPEN_FILE_WRITE] = "Can't open file for write",
        [EX_READ_IMG_NULL] = "Reading image from bmp ended failure. Pointers are null.",
        [EX_READ_IMG_INVALID_HEADER] = "Reading image from bmp ended failure. Invalid header of bmp.",
        [EX_READ_IMG_INVALID_BITS] = "Reading image from bmp ended failure. Invalid bits of pixels.",
        [EX_INVALID_POSITION_SET] = "Reading image from bmp ended failure. Invalid position setting.",
        [EX_WRITE_IMG_NULL] = "Writing image from bmp ended failure. Pointers are null.", 
        [EX_WRITE_HEADER_ERROR] = "Writing image from bmp ended failure. Cannot write header.",
        [EX_WRITE_IMG_ERROR] = "Writing image from bmp ended failure.",
        [EX_WRONG_ARGS] = "Invalid number of arguments."
};


void print_err(enum error err) {
    fprintf(stderr, "Err: %s\n", errors_desc[err]);
}

int main(int argc, char* argv[]) {
    
    if (argc != 3){
        print_err(EX_WRONG_ARGS);
        return 1;
    }

    struct image img = {0};
    struct image new = {0};

    char* source_image_name = argv[1];
    char* transformed_image_name = argv[2];

    switch (from_bmp_with_file_name(source_image_name, &img)){
        case READ_NULL:
            print_err(EX_READ_IMG_NULL);
            return 1;
        case READ_INVALID_HEADER:
            print_err(EX_READ_IMG_INVALID_HEADER);
            return 1;
        case READ_INVALID_POSITION_SET:
            print_err(EX_INVALID_POSITION_SET);
            return 1;
        case READ_INVALID_BITS:
            print_err(EX_READ_IMG_INVALID_BITS);
            return 1;
        case READ_FILE_ERROR:
            print_err(EX_DESCRIPTION_CANNOT_OPEN_FILE_READ);
            return 1;
        case READ_OK:
            break;
    }

    new = apply_transf(img, rotate_left_90);

    switch (to_bmp_with_file_name(transformed_image_name, &new)) {
        case WRITE_NULL:
            print_err(EX_WRITE_IMG_NULL);
            break;
        case WRITE_HEADER_ERROR:
            print_err(EX_WRITE_HEADER_ERROR);
            break;
        case WRITE_BITS_ERROR:
            print_err(EX_WRITE_IMG_ERROR);
            break;
        case WRITE_FILE_ERROR:
            print_err(EX_DESCRIPTION_CANNOT_OPEN_FILE_WRITE);
            break;
        case WRITE_OK:
            break;
    }

    image_free(&img);
    image_free(&new);
    
    return 0;
}
