#include "../include/formats_handlers/bmp_format_handler.h"
#include <inttypes.h>


////////header settings

static const uint32_t SIGNATURE = 19778;
static const uint32_t RESERVED = 0;
static const uint32_t HEADER_SIZE = 40;
static const uint16_t PLANES = 1;
static const uint32_t COMPRESSION = 0;
static const uint32_t PIXEL_PER_M = 2834;
static const uint32_t COLORS_USED = 0;
static const uint32_t COLORS_IMPORTANT = 0;
static const size_t BIT_COUNT = 24;

#pragma pack(push, 1)
struct bmp_header {
    uint16_t signature;
    uint32_t filesize;
    uint32_t reserved;
    uint32_t data_offset;
    uint32_t size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bit_count;
    uint32_t compression;
    uint32_t image_size;
    uint32_t x_pixels_per_m;
    uint32_t y_pixels_per_m;
    uint32_t colors_used;
    uint32_t colors_important;
};
#pragma pack(pop)


static size_t calculate_padding(size_t width) {
    if ((sizeof(struct pixel) * width) % 4 == 0) return 0;
    return 4 - (sizeof(struct pixel) * width) % 4;
}

static size_t calculate_image_size(struct image const* img) {
  return (img->width * sizeof(struct pixel) + calculate_padding(img->width)) * img->height;
}

static size_t calculate_file_size(struct image const* img) {
  return (uint64_t)(sizeof(struct bmp_header)
                        + ((sizeof(struct pixel) * img->width + calculate_padding(img->width)) * img->height));
}

////////deserialize

static bool read_bmp_header( FILE * const in, struct bmp_header * const header ){                       
    return fread(header, sizeof(struct bmp_header), 1, in);
}

static enum read_status read_pixels( FILE * const in, struct image * img){
  uint64_t h = img->height;
  uint64_t w = img->width;
  struct pixel* ptr = img->data;
  size_t padding = calculate_padding(w);

  for (uint64_t i = 0; i < h; i++) {
    size_t c = fread(ptr, sizeof(struct pixel), w, in);
    ptr += w; 
    if (c != w) {
      return READ_INVALID_BITS;
    }
    if (fseek(in, padding, SEEK_CUR)) return READ_INVALID_POSITION_SET;
  }

  return READ_OK;
}


////////serialize

static struct bmp_header create_header(struct image const* img) {
    return (struct bmp_header) {
            .signature = SIGNATURE,
            .image_size = calculate_image_size(img),
            .filesize = calculate_file_size(img),
            .reserved = RESERVED,
            .data_offset = sizeof(struct bmp_header),
            .size = HEADER_SIZE,
            .width = img->width,
            .height = img->height,
            .planes = PLANES,
            .bit_count = BIT_COUNT,
            .compression = COMPRESSION,
            .x_pixels_per_m = PIXEL_PER_M,
            .y_pixels_per_m = PIXEL_PER_M,
            .colors_used = COLORS_USED,
            .colors_important = COLORS_IMPORTANT,
    };
}


////////deserialize

enum read_status from_bmp( FILE * const in, struct image* img ){

  if(!in) return READ_NULL;

  struct bmp_header header;
  if (!read_bmp_header(in, &header)) {
    return READ_INVALID_HEADER;
  }

  if (fseek(in, header.data_offset, SEEK_SET)) return READ_INVALID_POSITION_SET;

  *img = image_create(header.height, header.width);
  enum read_status result = read_pixels(in, img);
  if (result != READ_OK) image_free(img);
  return result;
}

enum read_status from_bmp_with_file_name ( char* const file_name, struct image* img ) {
  FILE* in;
  if (!open_file(&in, file_name, read_mode)){
      return READ_FILE_ERROR;
  }
  enum read_status result = from_bmp(in, img);
  close_file(&in);
  return result;
}


////////serialize

enum write_status to_bmp( FILE * const out, struct image const* img ) {

   if (!out || !img || !img->data) {
    return WRITE_NULL;
  }

  struct bmp_header header = create_header(img);
  if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
     return WRITE_HEADER_ERROR;
  }

  size_t padding = calculate_padding(img->width);

  for (uint64_t i = 0; i < img->height; i++) {
    if (!fwrite(image_get_pixel_line(img, i), sizeof(struct pixel), img->width, out)){
      return WRITE_BITS_ERROR;
    }
    size_t z = 0;
    if (!fwrite(&z, padding, 1, out)) {
      return WRITE_BITS_ERROR;
    }
  }
  return WRITE_OK;
}

enum write_status to_bmp_with_file_name( char* const file_name, struct image const* img ) {
  FILE* out;
  if (!open_file(&out, file_name, write_mode)){
      return WRITE_FILE_ERROR;
  }
  enum write_status result = to_bmp(out, img);
  close_file(&out);
  return result;
}
