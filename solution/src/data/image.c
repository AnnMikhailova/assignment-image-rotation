#include "../include/data/image.h"

struct image image_create(uint64_t h, uint64_t w){
    struct image img = {.width = w, .height = h, 
        .data = malloc(sizeof(struct pixel) * h * w)};
    return img;
}

void image_free(struct image* img){
    free(img->data);
}

struct pixel image_get_pixel(struct image image, uint64_t row, uint64_t col){
    return image.data[col + row * image.width];
}

void image_set_pixel(struct image* image, uint64_t row, uint64_t col, struct pixel p){
    image->data[col + row * image->width] = p;
}

void image_set_pixel_line(struct image * image, uint64_t row, struct pixel * const p){
    for (uint64_t j = 0; j < image->width; j++){
        image_set_pixel(image, row, j, p[j]);
    }
}

struct pixel* image_get_pixel_line(struct image const* image, uint64_t row){
    return image->data + row * image->width;
}
