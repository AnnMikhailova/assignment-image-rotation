#include "../include/io/file_io_util.h"

bool open_file(FILE **file, char* name, char* mode) {
    *file = fopen(name, mode);
    if (!file){
    	return false;
    }
    return *file;
}

bool close_file(FILE** file) {
    if (!*file) {
    	return false;
    }
    if (fclose(*file)){
        return true;
    }
    return false;
}
