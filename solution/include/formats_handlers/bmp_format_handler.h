#ifndef H_BMP_FORMAT_HANDLER
#define H_BMP_FORMAT_HANDLER

#include "../data/image.h"
#include "../io/file_io_util.h"
#include <stdbool.h>
#include <stdio.h>

enum read_status {
  READ_OK = 0,
  READ_INVALID_POSITION_SET,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_NULL, 
  READ_FILE_ERROR
};

enum write_status  {
  WRITE_OK = 0,
  WRITE_HEADER_ERROR,
  WRITE_BITS_ERROR,
  WRITE_NULL,
  WRITE_FILE_ERROR
};

enum read_status from_bmp( FILE * const in, struct image* img );

enum write_status to_bmp( FILE * const out, struct image const* img );

enum write_status to_bmp_with_file_name ( char* const file_name, struct image const* img );

enum read_status from_bmp_with_file_name ( char* const file_name, struct image* img );

#endif
