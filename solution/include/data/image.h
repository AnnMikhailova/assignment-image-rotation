#ifndef IMAGE
#define IMAGE

#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image image_create(uint64_t h, uint64_t w);

void image_free(struct image* img);


struct pixel image_get_pixel(struct image image, uint64_t row, uint64_t col);

void image_set_pixel(struct image* image, uint64_t row, uint64_t col, struct pixel p);

void image_set_pixel_line(struct image* image, uint64_t row, struct pixel * const p);

struct pixel* image_get_pixel_line(struct image const* image, uint64_t row);

#endif
