#ifndef TRANSF_HANDLER
#define TRANSF_HANDLER

#include "../data/image.h"

struct transformation_interface {
  struct image (*transform) ( struct image source );
};

enum transf_type{
  rotate_left_90
};

struct image apply_transf( struct image source , enum transf_type type);

#endif
