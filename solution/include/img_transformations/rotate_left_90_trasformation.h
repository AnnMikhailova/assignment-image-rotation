#ifndef ROTATE
#define ROTATE

#include "../data/image.h"
#include "transf_handler.h"

struct image rotate ( struct image const source );

struct transformation_interface get_transformation_interface_rotation_90_left();

#endif
