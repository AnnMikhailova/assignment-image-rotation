#ifndef FILE_IO_UTIL
#define FILE_IO_UTIL

#include <stdbool.h>
#include <stdio.h>

static char * const read_mode = "rb";
static char * const write_mode = "wb";

bool open_file(FILE **file, char *name, char *mode);

bool close_file(FILE **file);

#endif
